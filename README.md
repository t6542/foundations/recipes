# Recipes

This is one part of the practice projects given by [Odin Project](https://www.theodinproject.com/).
This particular project is the very first one from the [Foundations](https://www.theodinproject.com/paths/foundations/courses/foundations) course.
Aim of this particular project is to practice the basics of html.

---

## Projects Purpose

Whilst this project is extremely simple, the practice part is not in the html itself, rather the whole surrounding ecosystem of web development. From project prep to dev ops to deployment, there is a lot to learn and see.

## Installation

There is nothing to install as it is just some html files.

## [Live preview](https://sad-agnesi-569191.netlify.app/)
